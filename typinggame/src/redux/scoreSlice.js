
import { createSlice } from '@reduxjs/toolkit';

const scoreSlice = createSlice({
  name: 'score',
  initialState: {
    best: 0,
    current: 0
  },
  reducers: {
    setBestScore: (state, action) => {
      const { best } = state;

      if (best <= action.payload) {
        state.best = action.payload;
      }
    },
    setCurrentScore: (state, action) => {
      state.current = action.payload

      if(state.current > state.best) {
        state.best = state.current
      }
    }
  }
})

export const { setBestScore, setCurrentScore } = scoreSlice.actions;
export default scoreSlice.reducer;
