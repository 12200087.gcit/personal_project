import { configureStore } from '@reduxjs/toolkit'
import scoreReducer from './scoreSlice'
import timeReducer from './timeSlice'
import userReducer from './userSlice'
import gameModeReducer from './gameModeSlice'
import levelScoreReducer from './levelScoreSlice'

export const store = configureStore({
  reducer: {
    score: scoreReducer,
    time: timeReducer,
    user: userReducer,
    gameMode: gameModeReducer,
    levelScore: levelScoreReducer
  }
})
