import { createSlice } from "@reduxjs/toolkit";

const gameModeSlice = createSlice({
  name: 'gameMode',
  initialState: {
    simple: true
  },
  reducers: {
    simpleMode: (state) => {
      state.simple = true
    },
    levelMode: (state) => {
      state.simple = false
    }
  }
})

export const { simpleMode, levelMode } = gameModeSlice.actions;
export default gameModeSlice.reducer;
