
import { createSlice } from '@reduxjs/toolkit';

const levelScoreSlice = createSlice({
  name: 'levelScore',
  initialState: {
    best: 0,
    current: 0
  },
  reducers: {
    setBestScoreForLevel: (state, action) => {
      const { best } = state;

      if (best <= action.payload) {
        state.best = action.payload;
      }
    },
    setCurrentScoreForLevel: (state, action) => {
      state.current = action.payload

      if(state.current > state.best) {
        state.best = state.current
      }
    }
  }
})

export const { setBestScoreForLevel, setCurrentScoreForLevel } = levelScoreSlice.actions;
export default levelScoreSlice.reducer;
