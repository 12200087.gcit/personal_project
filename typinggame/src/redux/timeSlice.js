
import { createSlice } from '@reduxjs/toolkit';

const timeSlice = createSlice({
  name: 'time',
  initialState: {
    counter: 60,
    running: true
  },
  reducers: {
    decrement: state => {
      if(state.running) {
        if (state.counter == 0) {
          state.running = false
          // state.counter = 60
        } else {
          state.counter -= 1;
        }
      }
    },
    reset: state => {
      state.counter = 60
    },
    startTime: state => {
      state.running = true
    },
    stopTime: state => {
      state.running = false
    }
  }
})

export const { decrement, reset, startTime, stopTime } = timeSlice.actions;
export default timeSlice.reducer;
