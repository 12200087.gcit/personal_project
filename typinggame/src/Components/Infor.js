import React from "react"
import { StyleSheet, TouchableOpacity, Image } from "react-native"
import { getStatusBarHeight } from "react-native-status-bar-height"
import { Ionicons } from '@expo/vector-icons';

export default function Information({onPress}){
    return(
        <TouchableOpacity onPress={onPress} style={styles.container}>         
            <Ionicons name="information-circle" size={35} color="black" />
        </TouchableOpacity>
    );
}
const styles = StyleSheet.create({
    container:{
        position:'absolute',
        top:7+ getStatusBarHeight(),
        right:6,
    },

    image:{
        width:24,
        height:24,
    }
})
