import React from "react";
import { Text, StyleSheet } from "react-native-paper";

const ErrorMessage = ({msg}) => (
  <Text style={{color: 'red', fontWeight: 'bold', marginTop: 10}}>{msg}</Text>
)

export default ErrorMessage;
