import React from "react"
import { StyleSheet, TouchableOpacity, Image } from "react-native"
import { getStatusBarHeight } from "react-native-status-bar-height"
import { Entypo } from '@expo/vector-icons';

export default function Help({ onPress }){
    return(
        <TouchableOpacity
            onPress={onPress} style={styles.container}>
            <Entypo name="help-with-circle" size={30} color="black" />
        </TouchableOpacity>
    );
}
const styles = StyleSheet.create({
    container:{
        position:'absolute',
        top:7+getStatusBarHeight(),
        left:170,
    },

    image:{
        width:24,
        height:24,
    }
})
