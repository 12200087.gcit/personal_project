import { StyleSheet, Text, View } from 'react-native'
import React from 'react';
import { Button } from 'react-native-paper';

const PrimaryButton = ({title, onPress}) => {
  return (
    <Button onPress = {onPress} style={styles.button}>
      <Text style ={styles.text}>{title}</Text>
    </Button>  
  );
}

export default PrimaryButton;

const styles = StyleSheet.create({
  button: {
    borderRadius: 12,
    backgroundColor: '#fc8803',
    width: 200,
    marginTop: 15
  },
  text:{
    fontSize: 18,
    fontWeight: 'bold',
    color: 'white'
  }
})