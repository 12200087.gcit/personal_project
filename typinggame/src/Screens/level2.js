import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TextInput, Alert, BackHandler } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { setCurrentScoreForLevel } from '../redux/levelScoreSlice';
import { Background, Counter, ScoreBoard, CurrentScore, BackButton  } from './Index';
import { reset, stopTime } from '../redux/timeSlice';
import { levelMode } from '../redux/gameModeSlice';

import firebase from 'firebase/app';
import "firebase/auth";
import "firebase/firestore"
import { firebaseConfig } from '../firebase/firebase-config';

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
} else {
  firebase.app();
}

// const words = [
//   'ཀི', 'ཁུ', 'གེ', 'ངོ', 'ཅི',  'ཆུ', 'ཇེ', 'ཉོ', 'ཏི', 'ཐུ', 'དེ', 'ནོ', 'པི', 'ཕུ', 'བེ', 'མོ', 'ཙི', 'ཚུ', 'ཛེ', 'ཝོ', 'ཞི', 'ཟུ', 'འེ', 'ཡོ', 'རི', 'ལུ', 'ཤེ', 'སོ', 'ཧི', 'ཨུ',
//   'ཀུ', 'ཁེ', 'གོ', 'ངི', 'ཅུ',  'ཆེ', 'ཇོ', 'ཉི', 'ཏུ', 'ཐེ', 'དོ', 'ནི', 'པུ', 'ཕེ', 'བོ', 'མི', 'ཙུ', 'ཚེ', 'ཛོ', 'ཝི', 'ཞུ', 'ཟེ', 'འོ', 'ཡི', 'རུ', 'ལེ', 'ཤོ', 'སི', 'ཧུ', 'ཨེ',
//   'ཀེ', 'ཁོ', 'གི', 'ངུ', 'ཅེ',  'ཆོ', 'ཇི', 'ཉུ', 'ཏེ', 'ཐོ', 'དི', 'ནུ', 'པེ', 'ཕོ', 'བི', 'མུ', 'ཙེ', 'ཚོ', 'ཛི', 'ཝུ', 'ཞེ', 'ཟོ', 'འི', 'ཡུ', 'རེ', 'ལོ', 'ཤི', 'སུ', 'ཧེ', 'ཨོ',
//   'ཀོ', 'ཁི', 'གུ', 'ངེ', 'ཅོ',  'ཆི', 'ཇུ', 'ཉེ', 'ཏོ', 'ཐི', 'དུ', 'ནེ', 'པོ', 'ཕི', 'བུ', 'མེ', 'ཙོ', 'ཚི', 'ཛུ', 'ཝེ', 'ཞོ', 'ཟི', 'འུ', 'ཡེ', 'རོ', 'ལི', 'ཤུ', 'སེ', 'ཧོ', 'ཨི',
//   'ཀི', 'ཁུ', 'གེ', 'ངོ', 'ཅི',  'ཆུ', 'ཇེ', 'ཉོ', 'ཏི', 'ཐུ', 'དེ', 'ནོ', 'པི', 'ཕུ', 'བེ', 'མོ', 'ཙི', 'ཚུ', 'ཛེ', 'ཝོ', 'ཞི', 'ཟུ', 'འེ', 'ཡོ', 'རི', 'ལུ', 'ཤེ', 'སོ', 'ཧི', 'ཨུ',
//   'ཀུ', 'ཁེ', 'གོ', 'ངི', 'ཅུ',  'ཆེ', 'ཇོ', 'ཉི', 'ཏུ', 'ཐེ', 'དོ', 'ནི', 'པུ', 'ཕེ', 'བོ', 'མི', 'ཙུ', 'ཚེ', 'ཛོ', 'ཝི', 'ཞུ', 'ཟེ', 'འོ', 'ཡི', 'རུ', 'ལེ', 'ཤོ', 'སི', 'ཧུ', 'ཨེ',
//   'ཀེ', 'ཁོ', 'གི', 'ངུ', 'ཅེ',  'ཆོ', 'ཇི', 'ཉུ', 'ཏེ', 'ཐོ', 'དི', 'ནུ', 'པེ', 'ཕོ', 'བི', 'མུ', 'ཙེ', 'ཚོ', 'ཛི', 'ཝུ', 'ཞེ', 'ཟོ', 'འི', 'ཡུ', 'རེ', 'ལོ', 'ཤི', 'སུ', 'ཧེ', 'ཨོ',
//   'ཀོ', 'ཁི', 'གུ', 'ངེ', 'ཅོ',  'ཆི', 'ཇུ', 'ཉེ', 'ཏོ', 'ཐི', 'དུ', 'ནེ', 'པོ', 'ཕི', 'བུ', 'མེ', 'ཙོ', 'ཚི', 'ཛུ', 'ཝེ', 'ཞོ', 'ཟི', 'འུ', 'ཡེ', 'རོ', 'ལི', 'ཤུ', 'སེ', 'ཧོ', 'ཨི',
// ]

const Level2 = ({ navigation }) => {

  const [categories, setCategories] = useState({categories: []}); 

  const words = categories.word
  
  const fetchServicesDetails = async () => {
    try{

     firebase.firestore()
      .collection("Level02")
      .get() 
      .then((querySnapshot) => { 
        querySnapshot.forEach((doc) => {
          // console.log(`${doc.id} => ${doc.data()}`);
          setCategories(doc.data())
        })
           
      });
        
    }catch(e){
      console.log(e)
    }
  }

  useEffect(()=>{
    fetchServicesDetails();
    },[]);


  const dispatch = useDispatch()

  const [input, setInput] = useState('')
  const [index, setIndex] = useState(0)
  const [displayedText, setDisplayedText] = useState('ཨོ')

  const levelScore = useSelector((state) => (state.levelScore))
  const time = useSelector((state) => (state.time))

  const handleTextChange = (char) => {
    dispatch(levelMode())
    if(input == displayedText && time.counter > 0) {
      setIndex(index + 1)
      setDisplayedText(words[index])
      dispatch(setCurrentScoreForLevel(levelScore.current + 1))
      setInput('')
    } else {
      setInput(char)
    }

    if(time.counter == 0 || index == words.length + 1) {
      dispatch(stopTime())
      dispatch(reset())
      dispatch(levelMode())
      navigation.navigate('GameOver')
    }
  }

  useEffect(() => {
    if(time.counter == 0) {
      dispatch(stopTime())
      dispatch(reset())
      dispatch(levelMode())
      navigation.navigate('GameOver')
    }
  })

  const backAction = () => {
    if(time.counter!=0){

      Alert.alert("Hold on!", "Are you sure, you want to exit game?", [
        {
          text: "Cancel",
          onPress: () => null,
          style: "cancel"
        },
        { text: "YES", onPress: () =>{
          dispatch(reset())
          navigation.navigate('LevelMode')
        } }

      ]);
      return true;
    }    
  }

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', backAction);

    return () =>
        BackHandler.removeEventListener('hardwareBackPress', backAction);
  }, []);

  const backIOS=()=>{
    Alert.alert("Hold on!", "Are you sure, you want to exit game?", [
      {
        text: "Cancel",
        onPress: () => null,
        style: "cancel"
      },
      { text: "YES", onPress: () =>{
        dispatch(reset())
        navigation.goBack()
      } }

    ]);
    return true;
  }

  return (
    <Background>  
      <BackButton onPress={()=>backIOS()}/>
      <Counter />
      <CurrentScore />

      <View style ={styles.inputContainer}>
        <Text style ={styles.display}>{displayedText}</Text>
        <TextInput
          value={input}
          style ={styles.textInput}
          placeholder = 'Type Here'
          onChangeText={handleTextChange}
        />
      </View>
    </Background>
  )
}

export default Level2

const styles = StyleSheet.create({
  display:{
    fontSize: 30,
    fontWeight: 'bold'
  },
  inputContainer:{
    marginTop:'17%',
    marginBottom: 110,
    justifyContent: 'center',
    alignItems: 'center',
  },
  // scoreLabel: {
  //   fontSize: 20,
  //   marginRight: 5,
  //   fontWeight: 'bold'
  // },
  textInput: {
    borderWidth: 1,
    height: 50,
    width: 350,
    textAlign: 'center',
  }
})
