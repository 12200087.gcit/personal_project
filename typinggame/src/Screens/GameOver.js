import React from 'react';
import { Text, StyleSheet, TouchableOpacity, View } from 'react-native' 

import { useDispatch, useSelector } from 'react-redux';

import { setCurrentScore } from '../redux/scoreSlice';
import { setCurrentScoreForLevel } from '../redux/levelScoreSlice';


import { reset, startTime } from '../redux/timeSlice';
import Background from './Background';
import ScoreBoard from './ScoreBoard';

import { Ionicons } from '@expo/vector-icons';

function GameOver({navigation}) {
  const dispatch = useDispatch(); 
  const user = useSelector((state) =>(state.user))

  const handleReplay = () => {
    dispatch(setCurrentScore(0))
    dispatch(setCurrentScoreForLevel(0))
    dispatch(startTime())
    navigation.goBack()
  }

  const onhome = () => {
    dispatch(setCurrentScore(0))
    dispatch(setCurrentScoreForLevel(0))
    dispatch(startTime())
    dispatch(reset())
    navigation.navigate('Mode')
  }

  return (
    <Background>
      <Text style={styles.txt}>Thank you for playing {user.name}</Text>
      <ScoreBoard />

      <View style={styles.view}>
        <TouchableOpacity onPress={handleReplay}>
          <Ionicons name="ios-reload-circle" size={60} color="black" />
        </TouchableOpacity>

        <TouchableOpacity onPress={onhome}>
          <Ionicons name="md-home" size={60} color="black" />
        </TouchableOpacity>
      </View>

      
    </Background>
  );
}

export default GameOver;

const styles = StyleSheet.create({
  txt:{
    fontSize:20,
    marginTop:10
  },

  view:{
    height:'33%',
    width:'90%',
    alignItems:'flex-end',
    // borderWidth:2,
    // borderColor:'red',
    flexDirection:'row',
    justifyContent:'space-around',
   
  }

})
