import React from 'react'
import {View, Text, Button, Image, StyleSheet, TouchableOpacity} from 'react-native'
import Onboarding from 'react-native-onboarding-swiper';

const Dots = ({selected}) => {
  let backgroundColor;

  backgroundColor = selected ? 'rgba(0, 0, 0, 0.8)' : 'rgba(0, 0, 0, 0.3)';

  return (
      <View 
          style={{
              width:6,
              height: 6,
              marginHorizontal: 3,
              backgroundColor
          }}
      />
  );
}

const Skip = ({...props}) => (
  <TouchableOpacity
      style={{marginHorizontal:10}}
      {...props}
  >
      <Text style={{fontSize:16}}>Skip</Text>
  </TouchableOpacity>
);

const Next = ({...props}) => (
  <TouchableOpacity
      style={{marginHorizontal:10}}
      {...props}
  >
      <Text style={{fontSize:16}}>Next</Text>
  </TouchableOpacity>
);

const Done = ({...props}) => (
  <TouchableOpacity
      style={{marginHorizontal:10}}
      {...props}
  >
      <Text style={{fontSize:16}}>Done</Text>
  </TouchableOpacity>
);

export default function OnboardingScreen({navigation}) {
  return (
    <Onboarding
        SkipButtonComponent={Skip}
        NextButtonComponent={Next}
        DoneButtonComponent={Done}
        DotComponent={Dots}
        onSkip={() => navigation.replace("Mode")}
        onDone={() => navigation.replace("Mode")}
        pages={[
          {
            backgroundColor: '#a6e4d0',
            // image: <Image style={{height:'55%', width:'98%'}} source={require('../../assets/key1.png')} />,
            title: 'Dzongkha keyboard',
            subtitle: "To play this game, you need Dzongkha keyboard installed \n&\nInternet connection ",
          },
          {
            backgroundColor: '#a6e4d0',
            // image: <Image source={require('../../assets/tenzin.jpeg')} />,
            title: 'User Guideline',
            subtitle: '* You have 60 seconds for each level.\n\n*On reaching the game screen, the timer will automatically get started.\n\n*After typing each word, you have to press the spacebar to proceed to the next word. ',
          },
          {
            backgroundColor: '#a6e4d0',
            // image: <Image source={require('../../assets/n.jpg')} />,
            title: 'Live the dream',
            subtitle: "Perfect way to make your dream come true!\nImprove your typing skills and accuracy!",
          },
        ]}
      />
    );
};


const styles = StyleSheet.create({
  container: {
    flex: 1, 
    alignItems: 'center', 
    justifyContent: 'center'
  },
});
