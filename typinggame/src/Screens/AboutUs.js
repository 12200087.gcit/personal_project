import React from 'react';
import { View, Text, StyleSheet,ImageBackground } from 'react-native'
import { LinearGradient } from 'expo-linear-gradient';
import { BackButton } from './Index';

const AboutUs = ({navigation}) => {
  return (
    <LinearGradient style ={styles.bg} colors ={[`#4169e1`,`#f5fffa`,'#ffa500']}>
       <ImageBackground 
        source={require('../../assets/dd.jpg')}
        resizeMode="cover"
        style={styles.backImg}
        imageStyle={styles.backgroundImage}
      />
    <View style={styles.container}>
      <Text style={{fontSize:24, textAlign:'center'}}>About App</Text>
      <Text style={{ textAlign:'center'}}>Druk-Typing Game is a first ever dzongkha language {'\n'}typing game that aims to improve and assist{'\n'} users in improving their typing skills in our native language.{'\n'}</Text>
      
      <Text style={{fontSize:24}}>Contact Us</Text>
      <Text>For any kind of enquires, feedbacks, appreciation,{'\n'}need of change in
      future plan are welcomed at {'\n\n    '} Kinga Penjor, 12200056.gcit@rub.edu.bt
      {'\n     '}Namgyal Wangchuk, 12200066.gcit@rub.edu.bt{'\n    '} Tandin jamtsho
      12200087.gcit@rub.edu.bt{'\n     '}Tenzin Tshomo, 12200090.gcit@rub.edu.bt{'\n'}</Text>

      <Text style={{fontSize:24, textAlign:'center'}}>Credit</Text>
      <Text style={{ textAlign:'center'}}><Text style={{fontWeight:'bold'}}>App logo use from :</Text> https://www.amazon.com/{'\n'}adroit-corporate-services-Golden-Keyboard/dp/B07GYWZBXP</Text>
    </View>

    <BackButton onPress={()=>navigation.goBack()}/>

    </LinearGradient>
  );
}
export default AboutUs;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor:'#EBA93F'
  },
  view:{
    // height:'10%',
    width : '98%',
    // borderWidth:2,
    // borderColor:'red',
    margin:'5%',
    flexDirection:'row',
    justifyContent:'space-around',
    alignItems:'center'
  },
  view1:{
    // height:'10%',
    width : '98%',
    // borderWidth:2,
    // borderColor:'red',
    margin:'5%',
    // flexDirection:'row',
    justifyContent:'center',
    alignItems:'center'
  },
  image:{
    height:85,
    width:85,
    borderRadius:50
  },
  text:{
    // fontFamily:'arial',
    // fontWeight:'normal',
    fontSize:15,
    textAlign:'center'
  },  bg: {
    flex: 1,
  },
  backImg: {
    width: '100%',
    height: '100%',
    position: 'absolute',
    alignItems: 'center'
  },
  backgroundImage: {
    opacity: 0.17,
  },
});