import React from 'react';
import { View } from 'react-native';

import Button from '../Components/PrimaryButton';
import Background from './Background';
import { BackButton } from './Index';

const LevelMode = ({navigation}) => {
  return (
    <Background>
      <BackButton onPress={()=>navigation.goBack()}/>
      <View style = {{paddingTop: 70}}>
        <Button title="ཨང་གྲང་།" onPress={() => navigation.navigate('Level1') } />
        <Button title="དབྱངས་བཞི།" onPress={() => navigation.navigate('Level2') } />
        <Button title="མགོ་ཅན་དང་ འདོགས་ཅན།" onPress={() => navigation.navigate('Level3') } />
        <Button title="མིང་ཚིག།" onPress={() => navigation.navigate('Level4') } />
      </View>
    </Background>
  )
}

export default LevelMode
