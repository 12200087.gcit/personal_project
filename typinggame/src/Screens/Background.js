import React from 'react';

import { StyleSheet,Text,Button, View, Image, ImageBackground, ScrollView, SafeAreaView } from 'react-native'
import { LinearGradient } from 'expo-linear-gradient';
import BackButton from '../Components/BackButton';
import Information from '../Components/Infor';

// const Background = (props, {navigation}) => {
function Background(props){

  return (
    <LinearGradient style ={styles.container} colors ={[`#4169e1`,`#f5fffa`,'#ffa500']}>
     
      <ImageBackground 
        source={require('../../assets/dd.jpg')}
        resizeMode="cover"
        style={styles.backImg}
        imageStyle={styles.backgroundImage}
      />
      
      {/* <SafeAreaView> */}
        <ScrollView>
          <View style ={styles.view}>
            <Image 
              source={require('../../assets/keyboard.png')}
              style = {styles.img}
            />
            {props.children}
          </View>
        </ScrollView>
      {/* </SafeAreaView> */}
    </LinearGradient>
    
  )
}

export default Background

const styles = StyleSheet.create({
  view:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 24,
    paddingTop:100
  },
  img: {
    height: 150,
    width: 150,
    borderRadius: 150,
    borderColor: 'black',
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center' 
  },
  backImg: {
    width: '100%',
    height: '100%',
    position: 'absolute',
    alignItems: 'center'
  },
  backgroundImage: {
    opacity: 0.17,
  },
  container: {
    flex: 1,
  }
})