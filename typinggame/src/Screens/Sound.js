import { StyleSheet, Text, View, TouchableOpacity, ImageBackground } from 'react-native'
import React from 'react'
import Button from '../Components/PrimaryButton'
import { Audio } from 'expo-av';
import { LinearGradient } from 'expo-linear-gradient';
import { BackButton } from './Index';

const Sound = ({navigation}) => {
  const [sound, setSound] = React.useState();

  async function playSound1() {
    // console.log('Loading Sound');
    const { sound } = await Audio.Sound.createAsync(
       require('../../assets/1.mp3')
    );
    setSound(sound);

    //console.log('Playing Sound');
    await sound.playAsync(); }

  async function playSound2() {
    // console.log('Loading Sound');
    const { sound } = await Audio.Sound.createAsync(
        require('../../assets/2.mp3')
    );
    setSound(sound);

    // console.log('Playing Sound');
    await sound.playAsync(); }
      
  async function playSound3() {
    // console.log('Loading Sound');
    const { sound } = await Audio.Sound.createAsync(
        require('../../assets/3.mp3')
    );
    setSound(sound);

    // console.log('Playing Sound');
    await sound.playAsync(); }

  async function playSound4() {
    // console.log('Loading Sound');
    const { sound } = await Audio.Sound.createAsync(
        require('../../assets/4.mp3')
    );
    setSound(sound);

    //console.log('Playing Sound');
    await sound.playAsync(); }

  async function playSound5() {
    // console.log('Loading Sound');
    const { sound } = await Audio.Sound.createAsync(
        require('../../assets/5.mp3')
    );
    setSound(sound);

    //console.log('Playing Sound');
    await sound.playAsync(); } 
    
  async function playSound6() {
    // console.log('Loading Sound');
    const { sound } = await Audio.Sound.createAsync(
        require('../../assets/6.mp3')
    );
    setSound(sound);

    //console.log('Playing Sound');
    await sound.playAsync(); }

  async function playSound7() {
    // console.log('Loading Sound');
    const { sound } = await Audio.Sound.createAsync(
        require('../../assets/7.mp3')
    );
    setSound(sound);
  
    //console.log('Playing Sound');
    await sound.playAsync(); }

  async function playSound8() {
    // console.log('Loading Sound');
    const { sound } = await Audio.Sound.createAsync(
        require('../../assets/8.mp3')
    );
    setSound(sound);

    //console.log('Playing Sound');
    await sound.playAsync(); }

  async function playSound9() {
    // console.log('Loading Sound');
    const { sound } = await Audio.Sound.createAsync(
        require('../../assets/9.mp3')
    );
    setSound(sound);

    //console.log('Playing Sound');
    await sound.playAsync(); }

  async function playSound10() {
    // console.log('Loading Sound');
    const { sound } = await Audio.Sound.createAsync(
        require('../../assets/10.mp3')
    );
    setSound(sound);

    //console.log('Playing Sound');
    await sound.playAsync(); }

  async function playSound11() {
    // console.log('Loading Sound');
    const { sound } = await Audio.Sound.createAsync(
        require('../../assets/11.mp3')
    );
    setSound(sound);

    //console.log('Playing Sound');
    await sound.playAsync(); }

  async function playSound12() {
    // console.log('Loading Sound');
    const { sound } = await Audio.Sound.createAsync(
        require('../../assets/12.mp3')
    );
    setSound(sound);

    //console.log('Playing Sound');
    await sound.playAsync(); }

  async function playSound13() {
    // console.log('Loading Sound');
    const { sound } = await Audio.Sound.createAsync(
        require('../../assets/13.mp3')
    );
    setSound(sound);

    //console.log('Playing Sound');
    await sound.playAsync(); }

  async function playSound14() {
    // console.log('Loading Sound');
    const { sound } = await Audio.Sound.createAsync(
        require('../../assets/14.mp3')
    );
    setSound(sound);

    //console.log('Playing Sound');
    await sound.playAsync(); }

  async function playSound15() {
    // console.log('Loading Sound');
    const { sound } = await Audio.Sound.createAsync(
        require('../../assets/15.mp3')
    );
    setSound(sound);

    //console.log('Playing Sound');
    await sound.playAsync(); }

  async function playSound16() {
    // console.log('Loading Sound');
    const { sound } = await Audio.Sound.createAsync(
        require('../../assets/16.mp3')
    );
    setSound(sound);

    //console.log('Playing Sound');
    await sound.playAsync(); }

  async function playSound17() {
    // console.log('Loading Sound');
    const { sound } = await Audio.Sound.createAsync(
        require('../../assets/17.mp3')
    );
    setSound(sound);

    //console.log('Playing Sound');
    await sound.playAsync(); }


  async function playSound18() {
    // console.log('Loading Sound');
    const { sound } = await Audio.Sound.createAsync(
        require('../../assets/18.mp3')
    );
    setSound(sound);

    //console.log('Playing Sound');
    await sound.playAsync(); }

  async function playSound19() {
    // console.log('Loading Sound');
    const { sound } = await Audio.Sound.createAsync(
        require('../../assets/19.mp3')
    );
    setSound(sound);

    //console.log('Playing Sound');
    await sound.playAsync(); }

  async function playSound20() {
    // console.log('Loading Sound');
    const { sound } = await Audio.Sound.createAsync(
        require('../../assets/20.mp3')
    );
    setSound(sound);

    //console.log('Playing Sound');
    await sound.playAsync(); }

  async function playSound21() {
    // console.log('Loading Sound');
    const { sound } = await Audio.Sound.createAsync(
        require('../../assets/21.mp3')
    );
    setSound(sound);

    //console.log('Playing Sound');
    await sound.playAsync(); }

  async function playSound22() {
    // console.log('Loading Sound');
    const { sound } = await Audio.Sound.createAsync(
        require('../../assets/22.mp3')
    );
    setSound(sound);

    //console.log('Playing Sound');
    await sound.playAsync(); }

  async function playSound23() {
    // console.log('Loading Sound');
    const { sound } = await Audio.Sound.createAsync(
        require('../../assets/23.mp3')
    );
    setSound(sound);

    //console.log('Playing Sound');
    await sound.playAsync(); }

        
  async function playSound24() {
    // console.log('Loading Sound');
    const { sound } = await Audio.Sound.createAsync(
        require('../../assets/24.mp3')
    );
    setSound(sound);

    //console.log('Playing Sound');
    await sound.playAsync(); }

  async function playSound25() {
    // console.log('Loading Sound');
    const { sound } = await Audio.Sound.createAsync(
        require('../../assets/25.mp3')
    );
    setSound(sound);

    //console.log('Playing Sound');
    await sound.playAsync(); }

    async function playSound26() {
      // console.log('Loading Sound');
      const { sound } = await Audio.Sound.createAsync(
          require('../../assets/26.mp3')
      );
      setSound(sound);
  
      //console.log('Playing Sound');
      await sound.playAsync(); }
    
    async function playSound27() {
      // console.log('Loading Sound');
      const { sound } = await Audio.Sound.createAsync(
          require('../../assets/27.mp3')
      );
      setSound(sound);
  
      //console.log('Playing Sound');
      await sound.playAsync(); }  

    async function playSound28() {
      // console.log('Loading Sound');
      const { sound } = await Audio.Sound.createAsync(
          require('../../assets/28.mp3')
      );
      setSound(sound);
  
      //console.log('Playing Sound');
      await sound.playAsync(); }
  
    async function playSound29() {
      // console.log('Loading Sound');
      const { sound } = await Audio.Sound.createAsync(
          require('../../assets/29.mp3')
      );
      setSound(sound);
  
      //console.log('Playing Sound');
      await sound.playAsync(); }

    async function playSound30() {
      // console.log('Loading Sound');
      const { sound } = await Audio.Sound.createAsync(
          require('../../assets/30.mp3')
      );
      setSound(sound);
  
      //console.log('Playing Sound');
      await sound.playAsync(); }

  React.useEffect(() => {
    return sound
      ? () => {
          // console.log('Unloading Sound');
          sound.unloadAsync(); }
      : undefined;
  }, [sound]);

  return (
    <LinearGradient style={styles.container} colors ={[`#4169e1`,`#f5fffa`,'#ffa500']}>
      <BackButton onPress={()=>navigation.goBack()}/>
      <View >
      <ImageBackground 
          source={require('../../assets/dragon.png')}
          resizeMode="cover"
          style={styles.backImg}
          imageStyle={styles.backgroundImage}
        />
        
          <Text style={styles.txt}>Click to learn Pronounciation</Text>

        <View style={styles.con}>
        <TouchableOpacity onPress={playSound1} style={styles.co}>
          <Text style={styles.text}>ཀ</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={playSound2} style={styles.co}>
          <Text style={styles.text}>ཁ</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={playSound3} style={styles.co}>
          <Text style={styles.text}>ག</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={playSound4} style={styles.co}>
          <Text style={styles.text}>ང</Text>
        </TouchableOpacity>

    
        </View>

        <View style={styles.con}>

        <TouchableOpacity onPress={playSound5} style={styles.co}>
          <Text style={styles.text}>ཅ</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={playSound6} style={styles.co}>
          <Text style={styles.text}>ཆ</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={playSound7} style={styles.co}>
          <Text style={styles.text}>ཇ</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={playSound8} style={styles.co}>
          <Text style={styles.text}>ཉ</Text>
        </TouchableOpacity>
        </View>

        <View style={styles.con}>

        <TouchableOpacity onPress={playSound9} style={styles.co}>
          <Text style={styles.text}>ཏ</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={playSound10} style={styles.co}>
          <Text style={styles.text}>ཐ</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={playSound11} style={styles.co}>
          <Text style={styles.text}>ད</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={playSound12} style={styles.co}>
          <Text style={styles.text}>ན</Text>
        </TouchableOpacity>
        </View>

        <View style={styles.con}>

        <TouchableOpacity onPress={playSound13} style={styles.co}>
          <Text style={styles.text}>པ</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={playSound14} style={styles.co}>
          <Text style={styles.text}>ཕ</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={playSound15} style={styles.co}>
          <Text style={styles.text}>བ</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={playSound16} style={styles.co}>
          <Text style={styles.text}>མ</Text>
        </TouchableOpacity>
        </View>
        <View style={styles.con}>

        <TouchableOpacity onPress={playSound17} style={styles.co}>
          <Text style={styles.text}>ཙ</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={playSound18} style={styles.co}>
          <Text style={styles.text}>ཚ</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={playSound19} style={styles.co}>
          <Text style={styles.text}>ཛ</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={playSound20} style={styles.co}>
          <Text style={styles.text}>ཝ</Text>
        </TouchableOpacity>
        </View>

        <View style={styles.con}>

        <TouchableOpacity onPress={playSound21} style={styles.co}>
          <Text style={styles.text}>ཞ</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={playSound22} style={styles.co}>
          <Text style={styles.text}>ཟ</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={playSound23} style={styles.co}>
          <Text style={styles.text}>འ</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={playSound24} style={styles.co}>
          <Text style={styles.text}>ཡ</Text>
        </TouchableOpacity>
        </View>

        <View style={styles.con}>

        <TouchableOpacity onPress={playSound25} style={styles.co}>
          <Text style={styles.text}>ར</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={playSound26} style={styles.co}>
          <Text style={styles.text}>ལ</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={playSound27} style={styles.co}>
          <Text style={styles.text}>ཤ</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={playSound28} style={styles.co}>
          <Text style={styles.text}>ས</Text>
        </TouchableOpacity>
        </View>
        <View style={styles.con}>

        <TouchableOpacity onPress={playSound29} style={styles.co}>
          <Text style={styles.text}>ཧ</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={playSound30} style={styles.co}>
          <Text style={styles.text}>ཨ</Text>
        </TouchableOpacity>

        </View>

        <View style={{alignItems:'center'}}>
        <Button title="Skip" onPress={() => navigation.replace('SimpleMode') } />
        </View>
          
      </View>
    </LinearGradient>
    
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 37,
  },
  co:{
    marginTop: 5,
    borderWidth: 1,
    borderColor: 'black',
    width: 50,
    height: 47,
    backgroundColor: '#ecf0f1',
    borderRadius:8
  },
  text:{
    fontSize: 30,
    textAlign: 'center'
  },
  con: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'flex-start',
    marginBottom: 20

  },
  txt: {
    fontSize:18,
    textAlign:'center',
    paddingBottom:15,
    fontWeight:"700",
    color:'white'
  },
  backImg: {
    width: '100%',
    height: '100%',
    position: 'absolute',
    alignItems: 'center'
  },
  backgroundImage: {
    opacity: 0.17,
  },
});

export default Sound
