import React from 'react';
import { Ionicons } from '@expo/vector-icons';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';

import { useDispatch, useSelector } from 'react-redux';

import { setBestScore } from '../redux/scoreSlice';
import { reset, startTime } from '../redux/timeSlice';

import Button from '../Components/PrimaryButton';
import Background from './Background';
import BackButton from '../Components/BackButton';
import Information from '../Components/Infor';
import Help from '../Components/Help';

const Mode = ({navigation}) => {
  const dispatch = useDispatch(); 
  const user = useSelector((state) =>(state.user))

  const handletyping = () => {
    dispatch(setBestScore(0))
    // dispatch(startTime())
    console.log('jiii')
    navigation.navigate('LevelMode')
  }
  // console.log(setBestScore)


  return (
    <Background>
      <BackButton onPress={()=>navigation.goBack()}/>
      <Help onPress={() => navigation.navigate('Help')}/>
      <Information onPress={() => navigation.navigate('AboutUs')}/>
      <View style = {styles.container}>
        <Text style={{fontSize:25, fontWeight:'bold'}}>Druk Typing Game</Text>
        <Text style = {styles.titleCase}>Welcome, {user.name}</Text>
        <Text style = {styles.txt}>Choose Mode</Text>
        
        <Button title="Basic" onPress={() => navigation.navigate('Sound') } />
        <Button title="Typing" onPress={() => handletyping() }/>
        
      </View>
    </Background>
  )
}

export default Mode

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    padding: 30,
    alignItems: 'center'
  },
  txt: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'black',
    paddingBottom: 20,
  },
  titleCase: {
    textTransform: 'capitalize',
    flex: 1,
    justifyContent: 'center',
    padding: 30,
    alignItems: 'center',
    fontSize:18
  }
})
