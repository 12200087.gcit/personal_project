import React, { useState } from 'react';
import { StyleSheet, TextInput, View } from 'react-native';
import { useDispatch } from 'react-redux';

import Button from '../Components/PrimaryButton';
import Background from './Background';
import ErrorMessage from '../Components/ErrorMessage';

import { setUser } from '../redux/userSlice';


const User = ({navigation}) => {
  const dispatch = useDispatch()

  const [username, setUsername] = useState("");
  const [error, setError] = useState('')

  const handleClick = () => {
    if(username == '') {
      setError('Please enter username.')
    } else {
      dispatch(setUser(username))
      navigation.navigate('Mode')
    }
  }

  return (
    <Background>
      <View style={styles.container}>
        <TextInput 
          label= "Simple Username" 
          placeholder='Enter username'
          style ={styles.formControl}
          onChangeText={setUsername}
        />
        <ErrorMessage msg={error} />
        <View style={{justifyContent:'center', alignItems:'center'}}>
          <Button title="Enter" onPress={handleClick} />
        </View>
      </View>
    </Background>
  )
}

export default User

const styles = StyleSheet.create({
  container: {
    marginTop: '30%',  
  },
  formControl: {
    borderColor: 'black',
    overflow: 'hidden',
    borderWidth: 1,
    padding: 10,
    width: 300,
  }
})
