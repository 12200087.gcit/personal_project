import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { StyleSheet, Text, ImageBackground } from "react-native";

import { decrement } from "../redux/timeSlice";


const Counter = ({minute = 1}) => {
  const dispatch = useDispatch()
  const time = useSelector((state) => state.time)

  useEffect(() => {
      let interval = setInterval(() => {
        if(time.running) {
          dispatch(decrement())
        }
      }, minute * 1000)
      return () => { clearInterval(interval) }
  }, []);

  return (
    <ImageBackground style={styles.clock} source={require('../../assets/cc.png')}>
      <Text style={styles.scoreLabel}>{time.counter}</Text>
    </ImageBackground>
    
  )
}
const styles = StyleSheet.create({
  scoreLabel: {
    fontSize: 20,
    marginLeft:18,
    marginTop:15
  },

  clock:{
    height:60,
    width:60
  }

})

export default Counter;
