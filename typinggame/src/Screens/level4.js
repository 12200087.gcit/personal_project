import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TextInput, Alert, BackHandler } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { setCurrentScoreForLevel } from '../redux/levelScoreSlice';
import { Background, Counter, ScoreBoard, CurrentScore, BackButton  } from './Index';
import { reset, stopTime } from '../redux/timeSlice';
import { levelMode } from '../redux/gameModeSlice';

import firebase from 'firebase/app';
import "firebase/auth";
import "firebase/firestore"
import { firebaseConfig } from '../firebase/firebase-config';
// import { Button } from 'react-native-paper';

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
} else {
  firebase.app();
}

// const words = [
//   'སློབ་གྲྦ', 'རྒྱལ་ཁབ', 'ན་གཞོན', 'ལམ་སྲོལ', 'ལྒང་ཕུ',  'མི་སེར', 'ཆོས་གྲོགས', 'ཡོངས་འབྲེལ', 'གཞུང', 'སྤྲོ་སྟོན', 'གྱོན་ཆས', 'བརྩེ་གདུང', 'ཨོ་རྒྱན', 'ཡ་མཚན', 'བཀྲ་ཤིས', 'སྨོན་ལམ', 'སངས་རྒྱས', 'བྱི་ལི', 'དྲུག', 'གཅིག', 'ཀླུ', 'དགྲ', 'སློབ་གྲྦ', 'སློབ་དཔོན', 'ཕྱུགཔོ', 'སྤྱང་ཀ', 'ཡོན་ཏན', 'སྲོལ', 'འབྲེལ་བ', 'གཅིག',
//   'སློབ་གྲྦ', 'རྒྱལ་ཁབ', 'ན་གཞོན', 'ལམ་སྲོལ', 'ལྒང་ཕུ',  'མི་སེར', 'ཆོས་གྲོགས', 'ཡོངས་འབྲེལ', 'གཞུང', 'སྤྲོ་སྟོན', 'གྱོན་ཆས', 'བརྩེ་གདུང', 'ཨོ་རྒྱན', 'ཡ་མཚན', 'བཀྲ་ཤིས', 'སྨོན་ལམ', 'སངས་རྒྱས', 'བྱི་ལི', 'དྲུག', 'གཅིག', 'ཀླུ', 'དགྲ', 'སློབ་གྲྦ', 'སློབ་དཔོན', 'ཕྱུགཔོ', 'སྤྱང་ཀ', 'ཡོན་ཏན', 'སྲོལ', 'འབྲེལ་བ', 'གཅིག',
//   'སློབ་གྲྦ', 'རྒྱལ་ཁབ', 'ན་གཞོན', 'ལམ་སྲོལ', 'ལྒང་ཕུ',  'མི་སེར', 'ཆོས་གྲོགས', 'ཡོངས་འབྲེལ', 'གཞུང', 'སྤྲོ་སྟོན', 'གྱོན་ཆས', 'བརྩེ་གདུང', 'ཨོ་རྒྱན', 'ཡ་མཚན', 'བཀྲ་ཤིས', 'སྨོན་ལམ', 'སངས་རྒྱས', 'བྱི་ལི', 'དྲུག', 'གཅིག', 'ཀླུ', 'དགྲ', 'སློབ་གྲྦ', 'སློབ་དཔོན', 'ཕྱུགཔོ', 'སྤྱང་ཀ', 'ཡོན་ཏན', 'སྲོལ', 'འབྲེལ་བ', 'གཅིག',
//   'སློབ་གྲྦ', 'རྒྱལ་ཁབ', 'ན་གཞོན', 'ལམ་སྲོལ', 'ལྒང་ཕུ',  'མི་སེར', 'ཆོས་གྲོགས', 'ཡོངས་འབྲེལ', 'གཞུང', 'སྤྲོ་སྟོན', 'གྱོན་ཆས', 'བརྩེ་གདུང', 'ཨོ་རྒྱན', 'ཡ་མཚན', 'བཀྲ་ཤིས', 'སྨོན་ལམ', 'སངས་རྒྱས', 'བྱི་ལི', 'དྲུག', 'གཅིག', 'ཀླུ', 'དགྲ', 'སློབ་གྲྦ', 'སློབ་དཔོན', 'ཕྱུགཔོ', 'སྤྱང་ཀ', 'ཡོན་ཏན', 'སྲོལ', 'འབྲེལ་བ', 'གཅིག',
//   'སློབ་གྲྦ', 'རྒྱལ་ཁབ', 'ན་གཞོན', 'ལམ་སྲོལ', 'ལྒང་ཕུ',  'མི་སེར', 'ཆོས་གྲོགས', 'ཡོངས་འབྲེལ', 'གཞུང', 'སྤྲོ་སྟོན', 'གྱོན་ཆས', 'བརྩེ་གདུང', 'ཨོ་རྒྱན', 'ཡ་མཚན', 'བཀྲ་ཤིས', 'སྨོན་ལམ', 'སངས་རྒྱས', 'བྱི་ལི', 'དྲུག', 'གཅིག', 'ཀླུ', 'དགྲ', 'སློབ་གྲྦ', 'སློབ་དཔོན', 'ཕྱུགཔོ', 'སྤྱང་ཀ', 'ཡོན་ཏན', 'སྲོལ', 'འབྲེལ་བ', 'གཅིག', 
// ]

const Level4 = ({ navigation }) => {

  const [categories, setCategories] = useState({categories: []}); 

  const words = categories.word

  const fetchServicesDetails = async () => {
    try{

     firebase.firestore()
      .collection("Level04")
      .get() 
      .then((querySnapshot) => { 
        querySnapshot.forEach((doc) => {
          // console.log(`${doc.id} => ${doc.data()}`);
          setCategories(doc.data())
        })
           
      });
        
    }catch(e){
      console.log(e)
    }
  }

  useEffect(()=>{
    fetchServicesDetails();
    },[]);


  const dispatch = useDispatch()

  const [input, setInput] = useState('')
  const [index, setIndex] = useState(0)
  const [displayedText, setDisplayedText] = useState('དཔལ')

  const levelScore = useSelector((state) => (state.levelScore))
  const time = useSelector((state) => (state.time))

  const handleTextChange = (char) => {
    dispatch(levelMode())
    if(input == displayedText && time.counter > 0) {
      setIndex(index + 1)
      setDisplayedText(words[index])
      dispatch(setCurrentScoreForLevel(levelScore.current + 1))
      setInput('')
    } else {
      setInput(char)
    }

    if(time.counter == 0 || index == words.length + 1) {  
      console.log(words.length)
      dispatch(stopTime())
      dispatch(reset())
      dispatch(levelMode())
      navigation.navigate('GameOver')
    }
  }

  useEffect(() => {
    if(time.counter == 0) {
      dispatch(stopTime())
      dispatch(reset())
      dispatch(levelMode())
      navigation.navigate('GameOver')
    }
  })

  const backAction = () => {
    if(time.counter!=0){

      Alert.alert("Hold on!", "Are you sure, you want to exit game?", [
        {
          text: "Cancel",
          onPress: () => null,
          style: "cancel"
        },
        { text: "YES", onPress: () =>{
          dispatch(reset())
          navigation.navigate('LevelMode')
        } }

      ]);
      return true;
    }    
  }

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', backAction);

    return () =>
      BackHandler.removeEventListener('hardwareBackPress', backAction);
  }, []);

  const backIOS=()=>{
    Alert.alert("Hold on!", "Are you sure, you want to exit game?", [
      {
        text: "Cancel",
        onPress: () => null,
        style: "cancel"
      },
      { text: "YES", onPress: () =>{
        dispatch(reset())
        navigation.goBack()
      } }

    ]);
    return true;
  }

  return (
    <Background>  
      <BackButton onPress={()=>backIOS()}/>
      <Counter />
      <CurrentScore />

      <View style ={styles.inputContainer}>
        <Text style ={styles.display}>{displayedText}</Text>
        <TextInput
          value={input}
          style ={styles.textInput}
          placeholder = 'Type Here'
          onChangeText={handleTextChange}
        />
      </View>
    </Background>
  )
}

export default Level4

const styles = StyleSheet.create({
  display:{
    fontSize: 30,
    fontWeight: 'bold'
  },
  inputContainer:{
    marginTop:'17%',
    marginBottom: 110,
    justifyContent: 'center',
    alignItems: 'center',
  },
  // scoreLabel: {
  //   fontSize: 20,
  //   marginRight: 5,
  //   fontWeight: 'bold'
  // },
  textInput: {
    borderWidth: 1,
    height: 50,
    width: 350,
    textAlign: 'center',
  }
})
