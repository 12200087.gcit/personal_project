import { StyleSheet, Text, View , ImageBackground} from 'react-native'
import React from 'react'
import { LinearGradient } from 'expo-linear-gradient';
import { getStatusBarHeight } from "react-native-status-bar-height"
import { BackButton } from './Index';

const HelpScreen = ({navigation}) => {
  return (
    <LinearGradient style={styles.container} colors ={[`#4169e1`,`#f5fffa`,'#ffa500']}>
      <ImageBackground 
          source={require('../../assets/dragon.png')}
          resizeMode="cover"
          style={styles.backImg}
          imageStyle={styles.backgroundImage}
        />
        <View style={{top:7+getStatusBarHeight(),  position:'absolute',}}>
          <Text style={{fontWeight:'bold',fontSize:28 }}>Help Desk</Text>
        </View>
          <Text style={{fontWeight:'bold', fontSize:24}}>Druk Typing Game{'\n'}</Text>
          <Text style={{fontWeight:'bold', fontSize:18}}>Prerequisite{'\n'}</Text>
          <Text style={{fontSize:15}}># Dzongkha keyboard{'\n'}</Text>
          <Text style={{fontSize:15}}># Internet connection{'\n'}</Text>
          <Text style={{fontWeight:'bold', fontSize:18}}>Game rules:{'\n'}</Text>
          <View style={styles.text}>
          <Text style={{fontStyle:'italic'}}>  *  Select mode, you have 60 seconds for each level.{'\n'}</Text>
          <Text style={{fontStyle:'italic'}}>  *  On entering game screen, the timer will get{'\n      '}start automatically. {'\n'}</Text>
          <Text style={{fontStyle:'italic'}}>  *  At the center of the screen is the word, you must write.{'\n'}</Text>
          <Text style={{fontStyle:'italic'}}>  *  After you write the correct word, tab the spacebar.{'\n'}</Text>
          <Text style={{fontStyle:'italic'}}>  *  Then a new word will be generated.{'\n'}</Text>
          <Text style={{fontStyle:'italic'}}>  *  There will be differents words depending on your level.{'\n'}</Text>
          <Text style={{fontStyle:'italic', textAlign:'center', fontSize:16}}>Perfect way to make your dream come true!{'\n'}Improve your typing skills and accuracy!</Text>
        </View>
      <BackButton onPress={()=>navigation.goBack()}/>
    </LinearGradient>
  )
}

export default HelpScreen

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems:'center',
    justifyContent:'center'
    // padding: 37,
  },
  
  backImg: {
    width: '100%',
    height: '100%',
    position: 'absolute',
    alignItems: 'center'
  },
  backgroundImage: {
    opacity: 0.17,
  },
})