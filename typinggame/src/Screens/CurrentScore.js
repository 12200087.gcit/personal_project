import React from 'react';
import { StyleSheet, Text, View } from 'react-native'
import { useSelector } from 'react-redux';

const CurrentScore = () => {
  const score = useSelector((state) => (state.score))
  const levelScore = useSelector((state) => (state.levelScore))
  const mode = useSelector((state) => (state.gameMode))
  
  return (
    <View style={styles.view}>
      <Text style ={styles.scoreLabel}> Current Score:{mode.simple} </Text>
      <Text style={styles.score}>{mode.simple ? score.current : levelScore.current}</Text>
    </View>
  )
}

export default CurrentScore

const styles = StyleSheet.create({
  scoreLabel: {
    fontSize: 18,
    marginRight: 5,
    fontWeight: 'bold',
    textAlign:'center'
  },
  view:{
    borderWidth:1,
    borderColor:'black',
    borderRadius:10,
    height:80,
    width:150,
    marginTop:15
  },
  score:{
    textAlign:'center',
    // margin:10,
    fontSize:30
  }
})
