import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TextInput, Alert, BackHandler } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { setCurrentScore } from '../redux/scoreSlice';
import { simpleMode } from '../redux/gameModeSlice';
import { Background, Counter, CurrentScore, BackButton  } from './Index';
import { reset, stopTime } from '../redux/timeSlice';

import firebase from 'firebase/app';
import "firebase/auth";
import "firebase/firestore"
import { firebaseConfig } from '../firebase/firebase-config';
// import { Button } from 'react-native-paper';

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
} else {
  firebase.app();
}

// const words = [
//   'ཀ',  'ཡ',  'ཇ', 'ག', 'ཉ',  'ཐ', 'ད', 'ཅ','ང', 'ན', 'ཧ','ཕ', 'ཞ', 'ཆ', 'པ','ཁ', 'ར', 'ཨ', 'བ',  'ས', 'ཟ',  'ཛ', 'མ', 'ཝ', 'ཏ', 'འ', 'ཙ', 'ཚ', 'ཤ', 'ལ', 
//   'ཀ',  'ཡ',  'ཇ', 'ག', 'ཉ',  'ཐ', 'ད', 'ཅ','ང', 'ན', 'ཧ','ཕ', 'ཞ', 'ཆ', 'པ','ཁ', 'ར', 'ཨ', 'བ',  'ས', 'ཟ',  'ཛ', 'མ', 'ཝ', 'ཏ', 'འ', 'ཙ', 'ཚ', 'ཤ', 'ལ',
//   'ཀ',  'ཡ',  'ཇ', 'ག', 'ཉ',  'ཐ', 'ད', 'ཅ','ང', 'ན', 'ཧ','ཕ', 'ཞ', 'ཆ', 'པ','ཁ', 'ར', 'ཨ', 'བ',  'ས', 'ཟ',  'ཛ', 'མ', 'ཝ', 'ཏ', 'འ', 'ཙ', 'ཚ', 'ཤ', 'ལ',
//   'ཀ',  'ཡ',  'ཇ', 'ག', 'ཉ',  'ཐ', 'ད', 'ཅ','ང', 'ན', 'ཧ','ཕ', 'ཞ', 'ཆ', 'པ','ཁ', 'ར', 'ཨ', 'བ',  'ས', 'ཟ',  'ཛ', 'མ', 'ཝ', 'ཏ', 'འ', 'ཙ', 'ཚ', 'ཤ', 'ལ',
//   'ཀ',  'ཡ',  'ཇ', 'ག', 'ཉ',  'ཐ', 'ད', 'ཅ','ང', 'ན', 'ཧ','ཕ', 'ཞ', 'ཆ', 'པ','ཁ', 'ར', 'ཨ', 'བ',  'ས', 'ཟ',  'ཛ', 'མ', 'ཝ', 'ཏ', 'འ', 'ཙ', 'ཚ', 'ཤ', 'ལ',
//   'ཀ',  'ཡ',  'ཇ', 'ག', 'ཉ',  'ཐ', 'ད', 'ཅ','ང', 'ན', 'ཧ','ཕ', 'ཞ', 'ཆ', 'པ','ཁ', 'ར', 'ཨ', 'བ',  'ས', 'ཟ',  'ཛ', 'མ', 'ཝ', 'ཏ', 'འ', 'ཙ', 'ཚ', 'ཤ', 'ལ',
//   'ཀ',  'ཡ',  'ཇ', 'ག', 'ཉ',  'ཐ', 'ད', 'ཅ','ང', 'ན', 'ཧ','ཕ', 'ཞ', 'ཆ', 'པ','ཁ', 'ར', 'ཨ', 'བ',  'ས', 'ཟ',  'ཛ', 'མ', 'ཝ', 'ཏ', 'འ', 'ཙ', 'ཚ', 'ཤ', 'ལ',
// ]

const SimpleMode = ({ navigation }) => {

  const [categories, setCategories] = useState({categories: []}); 

    const words = categories.word

    const fetchServicesDetails = async () => {
      try{
  
       firebase.firestore()
        .collection("Basic")
        .get() 
        .then((querySnapshot) => { 
          querySnapshot.forEach((doc) => {
            // console.log(`${doc.id} => ${doc.data()}`);
            setCategories(doc.data())
          })
             
        });
          
      }catch(e){
        console.log(e)
      }
    }
  
    useEffect(()=>{
      fetchServicesDetails();
      },[]);

  const dispatch = useDispatch()

  const [input, setInput] = useState('')
  const [index, setIndex] = useState(0)
  const [displayedText, setDisplayedText] = useState('ཤ')

  const score = useSelector((state) => (state.score))
  const time = useSelector((state) => (state.time))
  const mode = useSelector((state) => (state.gameMode)) 

  const handleTextChange = (char) => {
    dispatch(simpleMode())
    if(input == displayedText && time.counter > 0) {
      setIndex(index + 1)
      setDisplayedText(words[index])
      dispatch(setCurrentScore(score.current + 1))
      setInput('')
    } else {
      setInput(char)
    }

    if(time.counter == 0 ) { //|| index == words.length + 1
      dispatch(stopTime())
      dispatch(reset())
      dispatch(simpleMode())
      navigation.navigate('GameOver')
    }
  }

  useEffect(() => {
    if(time.counter == 0) {
      dispatch(stopTime())
      dispatch(reset())
      dispatch(simpleMode())
      navigation.navigate('GameOver')
    }
  })

  const backAction = () => {
    if(time.counter!=0){

      Alert.alert("Hold on!", "Are you sure, you want to exit game?", [
        {
          text: "Cancel",
          onPress: () => null,
          style: "cancel"
        },
        { text: "YES", onPress: () =>{
          dispatch(reset())
          navigation.navigate('Mode')
        } }

      ]);
      return true;
    }    
  }

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', backAction);

    return () =>
        BackHandler.removeEventListener('hardwareBackPress', backAction);
  }, []);

  const backIOS=()=>{
    Alert.alert("Hold on!", "Are you sure, you want to exit game?", [
      {
        text: "Cancel",
        onPress: () => null,
        style: "cancel"
      },
      { text: "YES", onPress: () =>{
        dispatch(reset())
        navigation.goBack()
      } }

    ]);
    return true;
  }

  return (
    <Background>  
      <BackButton onPress={()=>backIOS()}/>
      <Counter />
     {/* <Button onPress={backAction}>reset</Button> */}
      <CurrentScore/>

      <View style ={styles.inputContainer}>
        <Text style ={styles.display}>{displayedText}</Text>
        <TextInput
          value={input}
          style ={styles.textInput}
          placeholder = 'Type Here'
          onChangeText={handleTextChange}
        />
      </View>
    </Background>
  )
}

export default SimpleMode

const styles = StyleSheet.create({
  display:{
    fontSize: 30,
    fontWeight: 'bold'
  },
  inputContainer:{
    marginTop:'17%',
    marginBottom: 110,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textInput: {
    borderWidth: 1,
    height: 50,
    width: 350,
    textAlign: 'center',
  }
})
