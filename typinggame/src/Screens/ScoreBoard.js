import React from 'react';
import { StyleSheet, Text, View } from 'react-native'
import { useSelector } from 'react-redux';


const ScoreBoard = () => {
  const score = useSelector((state) => (state.score));
  const levelScore = useSelector((state) => (state.levelScore));
  const mode = useSelector((state) => (state.gameMode));
  
  return (
    <View style={styles.container}>
      <View style={styles.view}>
        <Text style ={styles.scoreLabel}> Current Score: </Text>
        <Text style={styles.score}>{ mode.simple ? score.current : levelScore.current}</Text>
      </View>
      <View style={styles.view}>
      <Text style ={styles.scoreLabel}> Best Score: </Text>
        <Text style={styles.score}>{ mode.simple ? score.best : levelScore.best } </Text>
      </View>
    </View>
  )
}

export default ScoreBoard

const styles = StyleSheet.create({
  container:{
    flexDirection:'row',
    marginTop:20

  },

  view:{
    borderWidth:2,
    borderColor:'black',
    borderRadius:10,
    height:100,
    width:130,
    margin:5
  },

  scoreLabel: {
    textAlign:'center',
    fontSize: 16,
    // marginRight: 5,
    paddingTop:10,
    fontWeight: 'bold'
  },

  score:{
    textAlign:'center',
    margin:10,
    fontSize:30

  }
})
