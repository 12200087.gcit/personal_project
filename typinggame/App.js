import { SafeAreaView, StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import OnboardingScreen from './src/Screens/OnBoarding';

import {
  SimpleMode,
  LevelMode,
  Level1,
  Level2,
  Level3,
  Level4,
  GameOver,
  UserName,
  Mode,
  Sound,
  AboutUs,
  HelpScreen
} from './src/Screens/Index'

const Stack = createStackNavigator();

import { store } from './src/redux/store';
import { Provider } from 'react-redux';

export default function App() {
  return (
    <Provider store={store}>
      <SafeAreaView style={{flex:1}}>
      <NavigationContainer>
        <Stack.Navigator
         initialRouteName='UserName'
         screenOptions = {{headerShown: false}}>
          <Stack.Screen name= 'UserName' component= {UserName}/>
          <Stack.Screen name= 'Mode' component= {Mode}/>

          {/* <Stack.Screen name= 'board' component= {OnboardingScreen}/> */}
          <Stack.Screen name= 'Help' component= {HelpScreen}/>

          <Stack.Screen name= 'Sound' component ={Sound}/>

          <Stack.Screen name= 'SimpleMode' component ={SimpleMode}/>
          <Stack.Screen name= 'LevelMode' component ={ LevelMode}/>
          <Stack.Screen name= 'AboutUs' component ={ AboutUs}/>

          <Stack.Screen name= 'Level1' component ={ Level1}/>
          <Stack.Screen name= 'Level2' component ={ Level2}/>
          <Stack.Screen name= 'Level3' component ={ Level3}/>
          <Stack.Screen name= 'Level4' component ={ Level4}/>

          <Stack.Screen name='GameOver' component={GameOver}/>
        </Stack.Navigator>
      </NavigationContainer>
      </SafeAreaView>
    </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
